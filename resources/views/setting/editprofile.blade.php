@extends('setting.setting')

@section('content2')

<!-- Content Wrapper. Contains page content -->
      
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h3>
            Edit Profile
            <small>advanced tables</small>
          </h3>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <table style="width:100%">
          <tr>
            <td class="right" width="25%">Item</td>
            <td class="center" width="5%">:</td> 
            <td class="left"width="70%">Kalkulus 1A</td>
          </tr>
          <tr>
            <td class="right" width="25%">Owner</td>
            <td class="center" width="5%">:</td> 
            <td class="left"width="70%">Abdullah Fikri</td>
          </tr>
          <tr>
            <td class="right" width="25%">Borrow Date</td>
            <td class="center" width="5%">:</td> 
            <td class="left"width="70%">19/4/2015-25/4/2015</td>
          </tr>
          <tr>
            <td class="right" width="25%">Share</td>
            <td class="center" width="5%">:</td> 
            <td class="left"width="70%">5 points x 12 days = 60 points</td>
          </tr>
          <tr>
            <td class="right" width="25%">Message</td>
            <td class="center" width="5%">:</td> 
            <td class="left"width="70%">
              <!-- <div class="form-group">
              <label for="comment">Comment:</label> -->
              <textarea class="form-control" rows="5" id="comment" placeholder="Tell something to the owner why you really need his goods.."></textarea>
            <!-- </div> -->
            </td>
          </tr>
        </table>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      <!-- /.content-wrapper -->

@endsection