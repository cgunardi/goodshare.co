@extends('index')

@section('content')

<div class="skin-black">
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">DASHBOARD</li>
      <li class="{{ ((Request::is('*/editprofile*'))? 'active' : '') }}" ><a href="{{url('setting/editprofile')}}"><i class="fa fa-user"></i> <span>Edit Profile</span></a></li>
      <li class="{{ ((Request::is('*/transactionlog*'))? 'active' : '') }}"><a href="{{url('setting/transactionlog')}}"><i class="fa fa-history"></i> <span>Transaction Log</span></a></li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
          <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Layout Options</span>
          <span class="label label-primary pull-right">4</span>
        </a>
        <ul class="treeview-menu">
          <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
          <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
          <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
          <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
        </ul>
      </li>
      <li>
        <a href="../widgets.html">
          <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
</div>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @yield('content2')
      </div><!-- /.content-wrapper -->

@endsection
@section('javascript')
  <script src="{{url()}}/assets/js/jquery.dataTables.min.js"></script>
  <script src="{{url()}}/assets/js/dataTables.bootstrap.min.js"></script>
  <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
        // $('.sidebar-menu li a').click(function(e) {

        //   $('.sidebar-menu li').removeClass('active');

        //   var $parent = $(this).parent();
        //   if (!$parent.hasClass('active')) {
        //       $parent.addClass('active');
        //   }
        //   e.preventDefault();
        // });
      });
    </script>
@endsection