@extends('setting.setting')

@section('content2')

<!-- Content Wrapper. Contains page content -->
      
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h3>
            Transaction Log
            <small>advanced tables</small>
          </h3>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Detail Transaction</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID Transaction</th>
                        <th>Date</th>
                        <th>Transaction Type</th>
                        <th>Goods</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>#GS21367</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21368</td>
                        <td>23 May 2015</td>
                        <td>Borrow</td>
                        <td>DSLR 60D</td>
                        <td><small class="label bg-yellow">Pending</small></td>
                      </tr>
                      <tr>
                        <td>#GS21369</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Mobil Honda Jazz Merah</td>
                        <td><small class="label bg-red">Rejected</small></td>
                      </tr>
                      <tr>
                        <td>#GS21370</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Suami Arin</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21371</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21372</td>
                        <td>23 June 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21381</td>
                        <td>23 May 2015</td>
                        <td>Borrow</td>
                        <td>DSLR 60D</td>
                        <td><small class="label bg-yellow">Pending</small></td>
                      </tr><tr>
                        <td>#GS21382</td>
                        <td>23 May 2015</td>
                        <td>Borrow</td>
                        <td>DSLR 60D</td>
                        <td><small class="label bg-yellow">Pending</small></td>
                      </tr>
                      <tr>
                        <td>#GS21373</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21374</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21375</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21376</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21377</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21378</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21379</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21380</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21385</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21382</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21383</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      <tr>
                        <td>#GS21384</td>
                        <td>23 April 2015</td>
                        <td>Lend</td>
                        <td>Playstation 4</td>
                        <td><small class="label bg-green">Approved</small></td>
                      </tr>
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      <!-- /.content-wrapper -->

@endsection