@extends('index')

@section('content')
<div class="container detail-wrapper">
	<div class="row detail">
		<div class="row detail-name">
			<div class="col-sm-6">
				<div>{{$goods->title}}</div>
			</div>
		</div>
		<div class="row">
		
			<div class="col-sm-5 detail-left">
				<div class="row detail-img">
					<img src="{{url()}}/assets/img/portfolio/1.jpg" class="detail-img-main">
				</div>
				<div class="row">
					<div class="col-sm-6 detail-img">
						<img src="{{url()}}/assets/img/portfolio/1.jpg" class="detail-img-small">	
					</div>
					<div class="col-sm-6 detail-img">
						<img src="{{url()}}/assets/img/portfolio/1.jpg" class="detail-img-small">
					</div>
				</div>
			</div>	
			<div class="col-sm-7 detail-right">
				<div class="row">
					<div class="col-sm-7 detail-points">
						Share : {{$goods->point}} points / day
					</div>
					<div class="col-sm-5">
						<div class="row">
							<div class="col-sm-4 detail-owner-left">
								<img src="{{url()}}/assets/img/sally-sm.jpg" class="detail-owner-img">
							</div>
							<div class="col-sm-8">
								<div class="detail-owner-name">
									{{$goods->owner()->name}}
								</div>
								<div class="detail-owner-level">
									Green Captain
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h3>Description :</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 detail-description">
						<p>{{$goods->description}}</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-9">
						<div class="input-group">
			                <div class="input-group-addon filter">
			                  <i class="fa fa-calendar"></i>
			                </div>
			                <input name="borrowdate" type="text" class="form-control pull-right filterdate" value="04/19/2015 - 04/25/2015" />
			            </div><!-- /.input group -->
		            </div>
		            <div class="col-sm-3">
		                <a data-toggle="modal" data-target="#request-now" class="btn btn-primary" id="init-search-btn">Request Now!</a>
		            </div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<hr id="detail-hline">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8 col-xs-8">
						<div class="inline-block review-font">
							<b><span class="review-number">{{$goods->review_count}}</span> Reviews &nbsp</b>
						</div>
						<div>
							<div class="star-rating rating-xs rating-enabled">
								<div class="rating-container rating-gly-star" data-content="">
									<div class="rating-stars" data-content="" style="width: 70%;">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-4 right review-font">
						<a data-toggle="modal" data-target="#write-review" class="btn btn-default"><i class="fa fa-pencil"></i> Write a Review</a>
					</div>
				</div>
				@foreach($goods->reviews() as $review)
				<div class="row">
					<div class="col-sm-12 detail-description">
						<div class="row">
						  	<div class="col-md-3 text-center row-space-2">
						    	<a href="/users/show/1571762" class="media-photo media-round row-space-1 text-center" name="review_26250613" target="blank">
						        	<img alt="Ploipailin" class="detail-owner-img" data-original="https://a0.muscache.com/ic/users/1571762/profile_pic/1406843871/original.jpg?interpolation=lanczos-none&amp;crop=w:w;*,*&amp;crop=h:h;*,*&amp;resize=68:*&amp;output-format=jpg&amp;output-quality=70" height="68" src="https://a0.muscache.com/ic/users/1571762/profile_pic/1406843871/original.jpg?interpolation=lanczos-none&amp;crop=w:w;*,*&amp;crop=h:h;*,*&amp;resize=68:*&amp;output-format=jpg&amp;output-quality=70" title="Ploipailin" width="68" style="display: inline;">
								</a>
								<div class="name">
						      		<a href="/users/show/1571762" class="text-muted link-reset" target="blank">Ploipailin</a>
						    	</div>
						  	</div>
							<div class="col-md-9">
							    <div class="row-space-2">
							      	<div data-review-id="26250613" data-original-text="" class="review-text expandable expandable-trigger-more expanded">
							        	<div class="expandable-content" style="transition: none; -webkit-transition: none;">
							          		<p>{{$review->text}}</p>
											<div class="expandable-indicator expandable-indicator-light"></div>
							        	</div>
										<!-- <a class="expandable-trigger-more text-muted" href="#">
							          		<strong>+ More</strong>
							        	</a> -->
							      	</div>
									<!-- <div class="text-muted review-subtext">
							        	<div class="review-translation-language" data-review-id="26250613">
							        	</div>
							          	<div class="date">February 2015</div>
							      	</div> -->
							    </div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>	
		</div>
	</div>
</div>

<div class="modal fade" id="request-now" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	    		<h4 class="modal-title">Are you sure?</h4>
	  		</div>
	  		<div class="modal-body">
	    		<table style="width:100%">
				  <tr>
				    <td class="right" width="25%">Item</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%">Kalkulus 1A</td>
				  </tr>
				  <tr>
				    <td class="right" width="25%">Owner</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%">Abdullah Fikri</td>
				  </tr>
				  <tr>
				    <td class="right" width="25%">Borrow Date</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%">19/4/2015-25/4/2015</td>
				  </tr>
				  <tr>
				    <td class="right" width="25%">Share</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%">5 points x 12 days = 60 points</td>
				  </tr>
				  <tr>
				    <td class="right" width="25%">Message</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%">
				    	<!-- <div class="form-group">
						  <label for="comment">Comment:</label> -->
						  <textarea class="form-control" rows="5" id="comment" placeholder="Tell something to the owner why you really need his goods.."></textarea>
						<!-- </div> -->
				    </td>
				  </tr>
				</table>
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	    		<button type="button" class="btn btn-primary">Request</button>
	  		</div>
		</div><!-- /.modal-content -->
	</div>
</div>
<div class="modal fade" id="write-review" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	    		<h4 class="modal-title">Write a Review</h4>
	  		</div>
	  		<div class="modal-body">
	    		<table style="width:100%">
				  <tr>
				    <td class="right" width="25%">Rating</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%"><input id="rating-input" type="number" /></td>
				  </tr>
				  <tr>
				    <td class="right" width="25%">Feedback</td>
				    <td class="center" width="5%">:</td> 
				    <td class="left"width="70%">
				    	<!-- <div class="form-group">
						  <label for="comment">Comment:</label> -->
						  <textarea class="form-control" rows="5" id="comment" placeholder="Tell something to the owner why you really need his goods.."></textarea>
						<!-- </div> -->
				    </td>
				  </tr>
				</table>
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	    		<button type="button" class="btn btn-primary">Send Review</button>
	  		</div>
		</div><!-- /.modal-content -->
	</div>
</div>
@endsection
