@extends('index')

@section('content')
<div class="container list-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<h3 style="color:black">Goodshare Feedback</h3>
		</div>

	</div>
	<div class="row list">
		<div class="col-sm-12 col-md-6">
			<h4> Share Log </h4>
			<table style="width:100%" class="left">
			  <tr>
			    <td width="20%">ID Transaction</td>
			    <td width="5%">:</td> 
			    <td width="75%">#D1432071</td>
			  </tr>
			  <tr>
			    <td>Item</td>
			    <td>:</td> 
			    <td>Kalkulus 1A</td>
			  </tr>
			  <tr>
			    <td>Owner</td>
			    <td>:</td> 
			    <td>Abdullah Fikri</td>
			  </tr>
			  <tr>
			    <td>Borrow Date</td>
			    <td>:</td> 
			    <td>19/4/2015-25/4/2015</td>
			  </tr>
			  <tr>
			    <td>Share Points</td>
			    <td>:</td> 
			    <td>5 points x 12 days = 60 points</td>
			  </tr>
			</table>
		</div>
		<div class="col-sm-12 col-md-6">
			<h4> Goods Review </h4>
			<table style="width:100%" class="left">
			  <tr>
			    <td width="20%">Rating</td>
			    <td width="5%">:</td> 
			    <td width="75%"><input id="rating-input" type="number" /></td>
			  </tr>
			  <tr>
			    <td valign="top">Feedback</td>
			    <td valign="top">:</td> 
			    <td><textarea class="form-control" rows="5" id="comment" placeholder="Tell something to the owner about your experienced of his goods.."></textarea></td>
			  </tr>
			</table>
		</div>
		<div class="col-sm-12 center">
			<a class="btn btn-primary" id="init-search-btn">Send Review</a>
		</div>
		
	</div>
	<!-- <div class="row catalog">
		<div class="col-sm-4">
			<img src="assets/img/portfolio/4.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/5.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/6.jpg" class="catalog-img">
		</div>
	</div> -->
</div>
@endsection
