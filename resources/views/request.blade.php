@extends('index')

@section('content')
<div class="container list-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<h3 style="color:black">Request</h3>
		</div>
	</div>
	<div class="row list">
		<div class="col-sm-4">
			<a href="#">
				<div class="row catalog-img" style="background-image: url(assets/img/portfolio/1.jpg)">
					<div class="row list-info-top">
						<div class="row catalog-name">
							Kalkulus 1A
						</div>
					</div>
					<div class="row request-box-waiting">
						<div class="row request-status-waiting">
							Waiting
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="#">
				<div class="row catalog-img" style="background-image: url(assets/img/portfolio/2.jpg)">
					<div class="row list-info-top">
						<div class="row catalog-name">
							Playstation 4
						</div>
					</div>
					<div class="row request-box-rejected">
						<div class="row request-status-rejected">
							Maybe Next Time
						</div>
					</div>
				</div>
			</a>
		</div>
		
		<!-- <div class="col-sm-4">
			<img src="assets/img/portfolio/2.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/3.jpg" class="catalog-img">
		</div> -->
	</div>
	<!-- <div class="row catalog">
		<div class="col-sm-4">
			<img src="assets/img/portfolio/4.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/5.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/6.jpg" class="catalog-img">
		</div>
	</div> -->
</div>
@endsection
