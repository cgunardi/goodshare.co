@extends('index')

@section('content')
<div class="container list-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<h3 style="color:black">List</h3>
		</div>
	</div>
	<div class="row list">
		<div class="col-sm-4">
			<a href="#">
				<div class="row catalog-img" style="background-image: url(assets/img/portfolio/1.jpg)">
					<div class="row list-info-top">
						<div class="row catalog-name">
							Kalkulus 1A
						</div>
					</div>
					<div class="row list-box-borrowed">
						<div class="row list-status-borrowed">
							<div>
									<h4><b>Borrowed</b></h4>
							</div>
							<div class="info-borrowed">
									by: Fira Karinsha
							</div>
							<div class="info-borrowed">
									until:24/4/2015
							</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="#">
				<div class="row catalog-img" style="background-image: url(assets/img/portfolio/2.jpg)">
					<div class="row list-info-top">
						<div class="row catalog-name">
							Kalkulus 1A
						</div>
					</div>
					<div class="row list-box-available">
						<div class="row list-status-available">
							Available
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-4">
			<a href="#">
				<div class="row catalog-img" style="background-image: url(assets/img/placeholder-img.png)">
					<div class="row list-add">
						+ Add New List
					</div>
				</div>
			</a>
		</div>
		
		<!-- <div class="col-sm-4">
			<img src="assets/img/portfolio/2.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/3.jpg" class="catalog-img">
		</div> -->
	</div>
	<!-- <div class="row catalog">
		<div class="col-sm-4">
			<img src="assets/img/portfolio/4.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/5.jpg" class="catalog-img">
		</div>
		<div class="col-sm-4">
			<img src="assets/img/portfolio/6.jpg" class="catalog-img">
		</div>
	</div> -->
</div>
@endsection
