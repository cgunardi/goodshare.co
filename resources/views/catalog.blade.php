@extends('index')

@section('content')
<div class="container catalog-wrapper">
	<div class="row">
		<div class="col-md-7 init-search">
			<div class="inner-addon left-addon">
				<i class="glyphicon glyphicon-search"></i>
				<input type="text" class="form-control" placeholder="Search.." />
			</div>
		</div>
		<div class="col-md-2 init-search center">
			<select class="btn selectpicker">
				<option>All Categories</option>
				@foreach($categories as $category)
					<option><b>{{$category->name}}</b></option>
					@foreach($category->childs() as $child)
						<option>{{$child->name}}</option>
					@endforeach
				@endforeach
			</select>
		</div>
		<div class="col-md-2 init-search inline-block">
			<select class="btn selectpicker">
				<option>Bandung</option>
				<option>Jakarta</option>
				<option>Surabaya</option>
			</select>
		</div>
		<div class="col-md-1 init-search right">
			<a class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<span>Points Range : </span>
			<input id="range_2" type="text" name="range_2" value=""/>
		</div>
		<div class="col-md-6">
			<span>Distance Range : </span>
			<input id="range_1" type="text" name="range_1" value=""/>
		</div>
	</div>
	<div class="row catalog">
		@foreach($all_goods as $goods)
			<div class="col-sm-4">
				<a href="{{action('CatalogController@getGoods',$goods->id)}}">
					<div class="row catalog-img" style="background-image: url(assets/img/portfolio/{{ (($goods['id']%5)+1) }}.jpg)">
						<div class="row catalog-info-top">
							<div class="row catalog-name">
								{{$goods->title}}
							</div>
							<div class="row catalog-review">
								{{$goods->review_count}} Review
							</div>
						</div>
						<div class="row catalog-info-bottom">
							<div class="row catalog-point">
								{{$goods->point}} Points
							</div>
						</div>
					</div>
				</a>
			</div>
		@endforeach
	</div>
</div>
@endsection
