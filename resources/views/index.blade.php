<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Goodshare.co</title>

	<!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="{{url()}}/assets/css/bootstrap.min.css" type="text/css">

	<!-- Custom Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Merriweathe r:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{url()}}/assets/font-awesome/css/font-awesome.min.css" type="text/css">

	<!-- Plugin CSS -->
	<link rel="stylesheet" href="{{url()}}/assets/css/animate.min.css" type="text/css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="{{url()}}/assets/css/creative.css" type="text/css">
	<link rel="stylesheet" href="{{url()}}/assets/css/star-rating.css" type="text/css">
	<link rel="stylesheet" href="{{url()}}/assets/css/slider.css" type="text/css">
	<link rel="stylesheet" href="{{url()}}/assets/css/main.css" type="text/css">
	
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/assets/css/ion.rangeSlider.css" type="text/css">
    <link rel="stylesheet" href="{{url()}}/assets/css/ion.rangeSlider.skinNice.css" type="text/css">
    <link rel="stylesheet" href="{{url()}}/assets/css/slider.css" type="text/css">
    <link rel="stylesheet" href="{{url()}}/assets/css/AdminLTE.min.css" type="text/css">
    <link rel="stylesheet" href="{{url()}}/assets/css/_all-skins.min.css" type="text/css">
    <link rel="stylesheet" href="{{url()}}/assets/css/dataTables.bootstrap.css" type="text/css">
    
	<!-- Daterange picker -->

	<link href="{{url()}}/assets/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

	@yield('stylesheet')


</head>
	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"><div id="logos"></div></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a class="page-scroll" href="#portfolio">List</a>
					</li>
					@if (Auth::guest())
						<li>
							<a class="page-scroll" href="#services">Sign Up</a>
						</li>
					@else
						<li>
							<a class="page-scroll">Welcome {{ Auth::user()->name }} !</a>
						</li>
						<li>
							<a class="page-scroll" href="{{action('Auth\AuthController@getLogout')}}">Logout</a>
						</li>
					@endif
					<!-- <li>
						<a class="page-scroll" href="#contact">
							<img src="assets/img/sally-sm.jpg" width="25" height="25">
							Christian Gunardi
						</a>
					</li> -->
				</ul>
				@if (Auth::guest())
					{!! Form::open(array("class"=>"navbar-form navbar-right", "action"=>"Auth\AuthController@postLogin")) !!}
						<div class="form-group">
							{!! Form::email('email', "", array("class"=>"form-control", "placeholder"=>"Email")) !!}
							{!! Form::password('password', array("class"=>"form-control", "placeholder"=>"Password")) !!}
							{!! Form::hidden('redirectPath', Request::url()) !!}
						</div>
						{!! Form::submit('Login', array("class"=>"btn btn-default")) !!}
					{!! Form::close() !!}
				@endif
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
<body id="page-top">

		@yield('sidebar')
		@yield('content')
	
	
	<!-- jQuery -->
	<script src="{{url()}}/assets/js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="{{url()}}/assets/js/bootstrap.min.js"></script>
	<!-- Plugin JavaScript -->
	<script src="{{url()}}/assets/js/jquery.easing.min.js"></script>
	<script src="{{url()}}/assets/js/jquery.fittext.js"></script>
	<script src="{{url()}}/assets/js/wow.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="{{url()}}/assets/js/creative.js"></script>
	<script src="{{url()}}/assets/js/main.js"></script>
	<script src="{{url()}}/assets/js/bootstrap-slider.js"></script>
<<<<<<< HEAD
	<script src="{{url()}}/assets/js/star-rating.js"></script>
	<script src="{{url()}}/assets/js/ion.rangeSlider.min.js"></script>
	<!-- datepicker -->
=======
    <script src="{{url()}}/assets/js/star-rating.js"></script>
    <script src="{{url()}}/assets/js/ion.rangeSlider.min.js"></script>
    <script src="{{url()}}/assets/js/app.min.js"></script>
    <!-- datepicker -->
>>>>>>> 8fc995a789f8ecbfb9434a8b9d2aeda4f6046e1f

	@yield('javascript')
	
</body>

</html>
