@extends('index')

@section('content')
{!! Form::open(array("action"=>"OwnerController@postCreateGoods")) !!}
<div class="container detail-wrapper">
	<div id="alert-field" class="alert alert-warning">
		<h4><i class="fa fa-warning"></i> Warning!</h4>
		Please fill in all of the required fields.
	</div>
	@if($errors->any())
		<div class="alert alert-warning">
			<h4><i class="fa fa-warning"></i> Warning!</h4>
			@foreach($errors->all() as $error)
				{{ $error }}<br/>
			@endforeach
		</div>
	@endif
	<div class="row detail">
		<div class="row">
			<div class="col-sm-5 detail-left">
				<div class="row detail-points">
					<b>Goods title :</b> {!! Form::text("title", null, array("placeholder"=>"Title","id"=>"input-name")) !!}
				</div>
				<div class="row detail-img">
					<img src="{{url()}}/assets/img/placeholder-img.png" class="detail-img-main">
				</div>
				<div class="row">
					<div class="col-sm-6 detail-img">
						<img src="{{url()}}/assets/img/placeholder-img.png" class="detail-img-small">	
					</div>
					<div class="col-sm-6 detail-img">
						<img src="{{url()}}/assets/img/placeholder-img.png" class="detail-img-small">
					</div>
				</div>
			</div>	
			<div class="col-sm-7">
				<div class="row pd detail-points">
					<div class="col-sm-2">
						<b>Share :</b>
					</div>
					<div class="col-sm-3">
						{!! Form::text("point", null, array("class"=>"edit-input","placeholder"=>"0","id"=>"input-points")) !!}
					</div>
					<div class="col-sm-5">
						{!! Form::submit("save",array("class"=>"btn btn-primary")) !!}
					</div>
				</div>
				<div class="row pd detail-points">
					<div class="col-sm-2">
						<b>Category :</b>
					</div>
					<div class="col-sm-10">
						{!! Form::select("category_id", App\Category::getTree()) !!}
					</div>
				</div>
				<div class="row pd">
					<div class="col-sm-12 detail-points">
						<b>Description :</b>
					</div>
				</div>
				<div class="row pd">
					<div class="col-sm-12 detail-description">
						{!! Form::textarea("description", null, array(
							"class"=>"form-control",
							"placeholder"=>"Short description about your goods..",
							"required",
							"rows"=>"5")) !!}
					</div>
				</div>
				<!-- <div class="row">
					<div class="col-sm-12">
						<hr id="detail-hline">
					</div>
				</div> -->
				<div class="row detail-points pd">
					<div class="col-sm-4">
						<b>Availability :</b>
						<div class="2-group">
							<div class="input-group-addon filter">
								<i class="fa fa-calendar"></i>
							</div>
							<input id="input-availability" name="borrowdate" type="text" class="form-control pull-right filterdate" value="04/19/2015 - 04/25/2015" required>
						</div><!-- /.input group -->
					</div>
					<div class="col-sm-8">
						<b>Meeting Point :</b>
						{!! Form::hidden("geo-lat", "", array("id"=>"geo-lat")) !!}
						{!! Form::hidden("geo-lang", "", array("id"=>"geo-lang")) !!}
						<input id="input-meetingpoint" class="controls" type="text" placeholder="Enter a location" required>
						<div id="map-canvas"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<a class="btn btn-success" id="save-edit">Save</a>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
{!! Form::close() !!}

<div class="modal fade" id="saves" tabindex="-1" role="dialog" aria-hidden="true">
	<form id="save-update">
		<div class="modal-dialog">
			<div class="modal-content">
		  		<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Are you sure?</h4>
		  		</div>
		  		<div class="modal-body">
					<table style="width:100%">
					  <tr>
						<td class="right" width="25%">Item</td>
						<td class="center" width="5%">:</td> 
						<td class="left"width="70%" id="edit-name">-</td>
					  </tr>
					  <tr>
						<td class="right" width="25%">Share</td>
						<td class="center" width="5%">:</td> 
						<td class="left"width="70%"><span id="edit-points">-</span> points/day</td>
					  </tr>
					  <tr>
						<td class="right" width="25%" valign="top">Description</td>
						<td class="center" width="5%" valign="top">:</td> 
						<td class="left"width="70%"><span id="edit-description"></span></td>
					  </tr>
					  <tr>
						<td class="right" width="25%">Availability</td>
						<td class="center" width="5%">:</td> 
						<td class="left"width="70%" id="edit-availability">-</td>
					  </tr>
					  <tr>
						<td class="right" width="25%">Meeting Point</td>
						<td class="center" width="5%">:</td> 
						<td class="left"width="70%" id="edit-meetingpoint"></td>
					  </tr>
					</table>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		    		<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
		  		</div>
			</div><!-- /.modal-content -->
		</div>
	</form>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{url()}}/assets/css/bootstrap-select.min.css">
@endsection

@section('javascript')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script src="{{url()}}/assets/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="{{url()}}/assets/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="{{url()}}/assets/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$('.filterdate').daterangepicker();

	var lat = -6.925990;
	var lng = 107.607459;
	$('#input-lat').val(lat);
	$('#input-lng').val(lng);

	function setLatLang(_lat, _lng) {
		lat = _lat;
		lng = _lng;
		$('#geo-lat').val(lat);
		$('#geo-lang').val(lng);
		console.log(lat);
		console.log(lng);
	}

	function initialize() {
		var mapOptions = {
			center: new google.maps.LatLng(0,0),
			zoom: 5
		};
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		var input = /** @type {HTMLInputElement} */(
			document.getElementById('input-meetingpoint'));

		// var types = document.getElementById('type-selector');
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push([]);

		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);

		var infowindow = new google.maps.InfoWindow();
		var marker = new google.maps.Marker({
			map:map,
			draggable:true,
			visible: true,
			position: new google.maps.LatLng(lat, lng)
		});
		console.log(marker);
		map.setCenter(new google.maps.LatLng(lat, lng));

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			infowindow.close();
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				window.alert("Autocomplete's returned place contains no geometry");
				return;
			}

			console.log(autocomplete);

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(17);  // Why 17? Because it looks good.
			}

			setLatLang(place.geometry.location.lat(), place.geometry.location.lng());

			marker.setPosition(place.geometry.location);

			var address = '';
			if (place.address_components) {
				address = [
					(place.address_components[0] && place.address_components[0].short_name || ''),
					(place.address_components[1] && place.address_components[1].short_name || ''),
					(place.address_components[2] && place.address_components[2].short_name || '')
				].join(' ');
			}

			infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			infowindow.open(map, marker);
		});
		autocomplete.setTypes([]);

		google.maps.event.addListener(marker,'dragend',function(event) {
			setLatLang(event.latLng.lat(),event.latLng.lng());
			console.log(event);
		});
	}


	google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection
