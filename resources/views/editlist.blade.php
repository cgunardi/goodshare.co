@extends('index')

@section('content')
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<div class="container detail-wrapper">
	<div id="alert-field" class="alert alert-warning">
	    <h4><i class="fa fa-warning"></i> Warning!</h4>
		Please fill in all of the required fields.
	</div>
	<div class="row detail">
		<div class="row detail-name">
				Goods title : <input type="text" placeholder=" name" id="input-name">
		</div>
		<div class="row">
		
			<div class="col-sm-5 detail-left">
				<div class="row detail-img">
					<img src="assets/img/placeholder-img.png" class="detail-img-main">
				</div>
				<div class="row">
					<div class="col-sm-6 detail-img">
						<img src="assets/img/placeholder-img.png" class="detail-img-small">	
					</div>
					<div class="col-sm-6 detail-img">
						<img src="assets/img/placeholder-img.png" class="detail-img-small">
					</div>
				</div>
			</div>	
			<div class="col-sm-7 detail-right">
				<div class="row">
					<div class="col-sm-7 detail-points">
						<div class="row">
							<div class="col-sm-4 mr-3">
								Share : 
							</div>
							<div class="col-sm-3">
								<input type="text" placeholder="0" id="input-points" class="edit-input">
							</div>
							<div class="col-sm-5">
								points/day
							</div>
						</div>
						
					</div>
					<div class="col-sm-5">
						<a class="btn btn-primary" id="save-edit">Save</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h3>Description :</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 detail-description">
						<textarea id="input-description" class="form-control" rows="5" placeholder="Short description about your goods.." required></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<hr id="detail-hline">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<h3>Availability :</h3>
						<div class="2-group">
			                <div class="input-group-addon filter">
			                	<i class="fa fa-calendar"></i>
			                </div>
			                <input id="input-availability" name="borrowdate" type="text" class="form-control pull-right filterdate" value="04/19/2015 - 04/25/2015" required>
			            </div><!-- /.input group -->
					</div>
					<div class="col-sm-8">
						<h3>Meeting Point :</h3>
						<input id="input-meetingpoint" class="controls" type="text" placeholder="Enter a location" required>
					    <div id="map-canvas"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						
		            </div>
				</div>
			</div>	
		</div>
	</div>
</div>

<div class="modal fade" id="saves" tabindex="-1" role="dialog" aria-hidden="true">
	<form id="save-update">
		<div class="modal-dialog">
			<div class="modal-content">
		  		<div class="modal-header">
		    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    		<h4 class="modal-title">Are you sure?</h4>
		  		</div>
		  		<div class="modal-body">
		    		<table style="width:100%">
					  <tr>
					    <td class="right" width="25%">Item</td>
					    <td class="center" width="5%">:</td> 
					    <td class="left"width="70%" id="edit-name">-</td>
					  </tr>
					  <tr>
					    <td class="right" width="25%">Share</td>
					    <td class="center" width="5%">:</td> 
					    <td class="left"width="70%"><span id="edit-points">-</span> points/day</td>
					  </tr>
					  <tr>
					    <td class="right" width="25%" valign="top">Description</td>
					    <td class="center" width="5%" valign="top">:</td> 
					    <td class="left"width="70%"><span id="edit-description"></span></td>
					  </tr>
					  <tr>
					    <td class="right" width="25%">Availability</td>
					    <td class="center" width="5%">:</td> 
					    <td class="left"width="70%" id="edit-availability">-</td>
					  </tr>
					  <tr>
					    <td class="right" width="25%">Meeting Point</td>
					    <td class="center" width="5%">:</td> 
					    <td class="left"width="70%" id="edit-meetingpoint"></td>
					  </tr>
					</table>
		  		</div>
		  		<div class="modal-footer">
		    		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		    		<button type="submit" class="btn btn-primary">Save</button>
		  		</div>
			</div><!-- /.modal-content -->
		</div>
	</form>
</div>
@endsection
