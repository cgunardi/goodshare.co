<?php namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Goods extends ReviewedModel {

	use SoftDeletes;

	protected $table = 'goods';

	protected $fillable = ['category_id', 'title', 'description', 'point', 'geo_lang', 'geo_lat', 'user_id'];

	protected $hidden = ['id'];

	protected $dates = ['deleted_at'];

	public function owner()
	{
		return $this->belongsTo('App\User', 'user_id')->first();
	}

	public function category()
	{
		return $this->belongsTo('App\Category', 'category_id')->first();	
	}

	public function reviews()
	{
		return $this->hasMany('App\GoodsReview', 'goods_id')->get();
	}

	public function availabilities()
	{
		return $this->hasMany('App\GoodsAvailability', 'goods_id')->get();
	}

	public function transactions()
	{
		return $this->hasMany('App\Transaction', 'goods_id')->get();
	}

	public function scopePopular($scope, $count = 10)
	{
		return $query->sortByDesc('transaction_count')->take($count);
	}

	public function view()
	{
		$this->view_count++;
		$this->save();
	}

	public function scopeList($scope, $page)
	{

	}
}
