<?php namespace App;

class Reputation {
	private static $reputations_call = [
		"Grey Captain",
		"Green Captain",
		"Red Captain",
		"Yellow Captain",
		"Navy Captain",
		"Blue Captain",
		"Pink Captain",
		"Aqua Captain",
		"Marron Captain",
		"Black Captain",
	]
	private static $reputation_limit = [
		5,
		10,
		20,
		40,
		80,
		160,
		320,
		640,
		1280
	]
}
?>