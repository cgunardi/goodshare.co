<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsAvailability extends Model {

	use SoftDeletes;

	protected $table = 'goods_availabilities';

	protected $fillable = ['goods_id', 'date', 'available'];

	protected $hidden = ['id'];

	protected $dates = ['deleted_at'];

	public function goods()
	{
		return $this->belongsTo('App\Goods', 'goods_id');
	}
}
