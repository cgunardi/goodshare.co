<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends ReviewedModel implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes;

	protected $table = 'users';

	protected $fillable = [
		'name',
		'email',
		'password',
		'geo_lat',
		'geo_lang',
		'phone_number',
		'city',
		'facebook_id',
		'google_id',
		'point_balance',
		'address_current',
		'province_current',
		'city_current',
		'address_official',
		'province_official',
		'city_official'
	];

	protected $hidden = ['password', 'remember_token'];

	protected $dates = ['deleted_at'];

}
