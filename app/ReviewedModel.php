<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewedModel extends Model {

	public function addRating($rating)
	{
		$this->review_count++;
		if ($this->review_count==1) {
			$this->rating = $rating;
		}
		else {
			$this->rating = (($this->rating*($this->review_count-1))+$rating)/$this->review_count;
		}
		return $this->save();
	}

	public function updateRating($oldrating, $newrating)
	{
		if ($oldrating!=$newrating) {
			$this->rating = (($this->rating*($this->review_count))+($newrating-$oldrating))/$this->review_count;
			return $this->save();
		}
		return true;
	}
	
}