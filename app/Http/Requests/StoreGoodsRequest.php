<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreGoodsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'category_id' => 'required|integer|exists:categories,id',
			'title' => 'required|string|max:256',
			'description' => 'required|string',
			'point' => 'required|integer|min:1',
			'geo_lang' => 'required|numeric',
			'geo_lat' => 'required|numeric'
		];
	}

	public function message()
	{
		return [
			'category_id.exists' => "Category not found"
		];
	}

}
