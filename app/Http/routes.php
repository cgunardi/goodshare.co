<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('list', 'GoodshareController@lists');
Route::get('request', 'GoodshareController@request');
Route::get('editlist', 'GoodshareController@editlist');
Route::get('profile', 'GoodshareController@profile');
Route::get('rating', 'GoodshareController@rating');
Route::get('home', 'GoodshareController@home');
// Route::get('setting', 'GoodshareController@setting');
// Route::get('transactionlog', 'GoodshareController@transactionlog');
// Route::get('editprofile', 'GoodshareController@editprofile');
Route::get('detailitem', 'GoodshareController@detailitem');
Route::controllers([
	'/catalog' => 'CatalogController',
	'/auth' => 'Auth\AuthController',
	'/password' => 'Auth\PasswordController',
	'/owner' => 'OwnerController',
	'/setting' => 'SettingController'
]);

Route::get('/', 'GoodshareController@index');
// Route::get('catalog', 'GoodshareController@catalog');


