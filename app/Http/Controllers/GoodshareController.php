<?php namespace App\Http\Controllers;

class GoodshareController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('index');
    }
    public function catalog()
    {
        return view('catalog');
    }
    public function home()
    {
        return view('home');
    }
    public function detailitem()
    {
        return view('detailitem');
    }
    public function lists()
    {
        return view('list');
    }
    public function request()
    {
        return view('request');
    }

    public function editlist()
    {
        return view('editlist');
    }
    public function profile()
    {
        return view('profile');
    }
    public function rating()
    {
        return view('rating');
    }
    public function setting()
    {
        return view('setting.setting');
    }
    public function transactionlog()
    {
        return view('setting.transactionlog');
    }
    public function editprofile()
    {
        return view('setting.editprofile');
    }

}
