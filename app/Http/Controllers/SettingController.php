<?php namespace App\Http\Controllers;

use App\Category;
use App\Request\StoreGoodsRequest;

class SettingController extends Controller {
	
	public function getEditprofile()
	{
		return view('setting.editprofile');
	}
	public function getTransactionlog()
	{
		return view('setting.transactionlog');
	}

}
