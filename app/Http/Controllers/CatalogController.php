<?php namespace App\Http\Controllers;

use App\Category;
use App\Goods;

class CatalogController extends Controller {
	
	public function getIndex($pages=1)
	{
		$categories = Category::root()->get();
		$all_goods = Goods::all();

		return view('catalog', [
			'categories' => $categories,
			'all_goods' => $all_goods
		]);
	}

	public function getGoods($id)
	{
		$goods = Goods::findOrFail($id);

		return view('detailitem', [
			'goods' => $goods
		]);
	}

}
