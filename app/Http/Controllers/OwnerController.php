<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreGoodsRequest;

class OwnerController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function getCreateGoods()
	{
		return view('owner.create-goods');
	}

	public function postCreateGoods(StoreGoodsRequest $request)
	{

	}

}
