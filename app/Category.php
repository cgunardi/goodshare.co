<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

	use SoftDeletes;

	protected $table = 'categories';

	protected $fillable = ['parent_category_id', 'name', 'icon', 'image_url', 'is_hot'];

	protected $hidden = ['password', 'remember_token'];

	protected $dates = ['deleted_at'];

	public function isHaveParent()
	{
		return $this->parent_category_id!=null;
	}

	public function parent()
	{
		if ($this->isHaveParent())
			return $this->belongsTo('App\Category', 'parent_category_id')->first();;
		return null;
	}

	public function childs()
	{
		return $this->hasMany('App\Category', 'parent_category_id')->get();
	}

	public function scopeHot($scope)
	{
		return $scope->where("is_hot", true);
	}

	public function scopeRoot($scope)
	{
		return $scope->where("parent_category_id", null);
	}

	public function scopeParent($scope, $parent_id)
	{
		return $scope->where("parent_category_id", $parent_id);
	}

	public static function getTree($roots = null)
	{
		$result = array();
		if (is_null($roots))
			$roots = Category::root()->get();
		foreach ($roots as $root) {
			$childs = $root->childs();
			$childs_arr = array();
			$childs_arr[$root->id] = $root->name;
			foreach ($childs as $child) {
				$childs_arr[$child->id] = $child->name;
			}
			$result[$root->name] = $childs_arr;
		}
		return $result;
	}
}
