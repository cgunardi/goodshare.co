<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReview extends Model {

	use SoftDeletes;

	protected $table = 'user_reviews';

	protected $fillable = ['reviewer_id', 'user_id', 'rating', 'text'];

	protected $hidden = ['id'];

	protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function reviewer()
	{
		return $this->belongsTo('App\User', 'reviewer_id');	
	}

	public static function create(array $options = array())
	{
		$exist = UserReview::where('user_id',$options['user_id'])->where('reviewer_id', $options['reviewer_id'])->first();
		if ($exist==null) {
			$item = parent::create($options);
			$user = $item->user()->first();
			$user->addRating($item->rating);
			return $item;
		}
		else {
			$user = $exist->user()->first();
			$user->updateRating($exist->rating, $options['rating']);
			$exist->rating = $options['rating'];
			$exist->save();
			return $exist;
		}
	}

	public function update(array $options = array())
	{
		parent::update($options);
		$oldrating = UserReview::find($this->id)->rating;
		$user = $this->user();
		return $user->updateRating($oldrating, $this->rating);
	}
}
