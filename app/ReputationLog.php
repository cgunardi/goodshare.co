<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReputationLog extends Model {

	use SoftDeletes;

	protected $table = 'reputation_logs';

	protected $fillable = ['transaction_id', 'user_id', 'related_user_id', 'reputation', 'type'];

	protected $hidden = ['id'];

	protected $dates = ['deleted_at'];

	public static $TYPE_BORROW = 'b';
	public static $TYPE_BORROWED = 'd';

	public function transaction()
	{
		if (!is_null($this->transaction_id))
		{
			return $this->hasOne('App\Transaction', 'transaction_id');
		}
		return null;
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function relatedUser()
	{
		if (!is_null($this->transaction_id))
		{
			return $this->hasOne('App\User', 'related_user_id');
		}
		return null;
	}

	public static function create(array $options = array())
	{
		$user = User::find($options['user_id']);
		$user->reputation += $options['reputation'];
		if ($user->save()) {
			parent::create($options);
		}
		return false;
	}

	public function update(array $options = array())
	{
		$user = $this->user();
		$oldpointlog = PointLog::find($this->id);
		if ($oldpointlog->reputation!=$this->reputation) {
			$delta = $oldpointlog->reputation - $this->reputation;
			$user->reputation += $delta;
			if ($user->save) {
				return parent::update($options);
			}
			return false;
		}
		else {
			return parent::update($options);
		}
	}

}
