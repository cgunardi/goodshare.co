<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsImage extends Model {

	use SoftDeletes;

	protected $table = 'goods_images';

	protected $fillable = ['goods_id', 'image_url', 'short_description'];

	protected $hidden = ['id'];

	public function goods()
	{
		return $this->belongsTo('App\Goods', 'goods_id');
	}
}
