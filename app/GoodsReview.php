<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsReview extends Model {

	use SoftDeletes;

	protected $table = 'goods_reviews';

	protected $fillable = ['goods_id', 'user_id', 'rating', 'text'];

	protected $hidden = ['id'];

	protected $dates = ['deleted_at'];

	public function owner()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function goods()
	{
		return $this->belongsTo('App\Goods', 'goods_id');	
	}

	public static function create(array $options = array())
	{
		$item = parent::create($options);
		$goods = $item->goods()->get()[0];
		return $goods->addRating($item->rating);
	}

	public function update(array $options = array())
	{
		parent::create($options);
		$oldrating = GoodsReview::find($this->id)->rating;
		$goods = $this->goods();
		return $goods->updateRating($oldrating, $this->rating);
	}
}
