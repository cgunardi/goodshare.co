<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Transaction extends Model {

	use SoftDeletes;

	protected $table = 'transactions';

	protected $fillable = ['goods_id', 'borrower_id', 'date_start', 'date_finish', 'message'];

	protected $dates = ['deleted_at'];

	private static $STATE_SUBMITTED = 'S';
	private static $STATE_APPROVED = 'P';
	private static $STATE_REJECTED = 'J';
	private static $STATE_ACCEPTED = 'C';
	private static $STATE_RETURNED = 'R';

	/*		Approve - Accept - Return
			/
	    Submit 
			\
			Reject
	*/

	public function borrower()
	{
		return $this->belongsTo('App\User', 'borrower_id');
	}

	public function goods()
	{
		return $this->belongsTo('App\Goods', 'goods_id');	
	}

	public function transactionReject()
	{
		if ($this->state==Transaction::$STATE_SUBMITTED) {
			$this->state = Transaction::$STATE_REJECTED;
			if ($this->save()) {
				return PointLog::create([
					'transaction_id' => $this->id,
					'user_id' => $this->borrower_id,
					'related_user_id' => $this->goods()->first()->user_id,
					'point' => $this->pointsNeeds(),
					'type' => PointLog::$TYPE_CANCELLING
				]);
			};
		};
		return false;
	}

	public function transactionApprove()
	{
		if ($this->state==Transaction::$STATE_SUBMITTED) {
			$this->state = Transaction::$STATE_APPROVED;
			return $this->save();
		};
		return false;
	}

	public function transactionAccept()
	{
		if ($this->state==Transaction::$STATE_APPROVED) {
			$this->state = Transaction::$STATE_ACCEPTED;
			$item = $this->save();
			if ($item) {
				PointLog::create([
					'transaction_id' => $this->id,
					'user_id' => $this->borrower_id,
					'related_user_id' => $this->goods()->first()->user_id,
					'point' => $this->pointsNeeds(),
					'type' => PointLog::$TYPE_BORROWED
				]);
			};
			return $item;
		};
		return false;
	}

	public function transactionReturn()
	{
		if ($this->state==Transaction::$STATE_ACCEPTED) {
			$this->state = Transaction::$STATE_RETURNED;
			$sv = $this->save();
			if ($sv) {
				$goods = $this->goods()->first();
				$goods->transaction_count++;
				$goods->save();
			}
			return $sv;
		};
		return false;
	}

	public static function create(array $options = array())
	{
		$item = parent::create($options);
		$item->borrower_reviewed = false;
		$item->owner_reviewed = false;
		$item->state = Transaction::$STATE_SUBMITTED;
		$item->trans_number = 
		$item->save();
		PointLog::create([
			'transaction_id' => $item->id,
			'user_id' => $item->borrower_id,
			'related_user_id' => $item->goods()->first()->user_id,
			'point' => $item->pointsNeeds()*-1,
			'type' => PointLog::$TYPE_BORROW
		]);
		return $item;
	}

	public function ownerReview(App\User $user, $rating, $text)
	{
		GoodsReview::create([
			'goods_id' => $this->goods_id,
			'user_id' => $user->id,
			'rating' => $rating,
			'text' => $text
		]);
		$this->owner_reviewed = true;
		return $this->save();
	}

	public function borrowerReview(App\User $user, $rating, $text)
	{
		$this->borrower_reviewed = true;
		return GoodsReview::create([
			'goods_id' => $this->goods_id,
			'user_id' => $user->id,
			'rating' => $rating,
			'text' => $text
		]);
	}

	public function pointsNeeds()
	{
		$start = Carbon::createFromFormat('Y-m-d', $this->date_start);
		$finish = Carbon::createFromFormat('Y-m-d', $this->date_finish);
		$days = $start->diffInDays($finish);
		return $days*$this->goods()->first()->point;
	}

	public function pointLogs()
	{
		return $this->hasMany('App\PointLog');
	}
}
