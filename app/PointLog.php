<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PointLog extends Model {

	use SoftDeletes;

	protected $table = 'point_logs';

	protected $fillable = ['transaction_id', 'user_id', 'related_user_id', 'point', 'type'];

	protected $hidden = ['id'];

	protected $dates = ['deleted_at'];

	public static $TYPE_BORROW = 'b';
	public static $TYPE_BORROWED = 'd';
	public static $TYPE_REFUND = 'r';
	public static $TYPE_TOPUP = 't';
	public static $TYPE_CANCELLING = 'c';

	public function transaction()
	{
		if (!is_null($this->transaction_id))
		{
			return $this->hasOne('App\Transaction', 'transaction_id');
		}
		return null;
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function relatedUser()
	{
		if (!is_null($this->transaction_id))
		{
			return $this->hasOne('App\User', 'related_user_id');
		}
		return null;
	}

	public static function create(array $options = array())
	{
		$user = User::find($options['user_id']);
		if (($options['point']>=0)||(($options['point']<0)&&($user->point_balance>=$options['point'])))
		{
			$user->point_balance += $options['point'];
			if ($user->save()) {
				parent::create($options);
			}
		}
		return false;
	}

	public function update(array $options = array())
	{
		$user = $this->user();
		$oldpointlog = PointLog::find($this->id);
		if ($oldpointlog->point!=$this->point) {
			$delta = $oldpointlog->point - $this->point;
			if (($delta>=0)||(($delta<0)&&($user->point_balance>=$delta)))
			{
				$user->point_balance += $delta;
				if ($user->save) {
					return parent::update($options);
				}
				return false;
			}
			return false;
		}
		else {
			return parent::update($options);
		}
	}

}
