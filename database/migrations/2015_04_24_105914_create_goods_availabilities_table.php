<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsAvailabilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goods_availabilities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('goods_id')->unsigned();
			$table->date('date');
			$table->boolean('available');

			$table->timestamps();

			$table->foreign('goods_id')->references('id')->on('goods')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goods_availabilities');
	}

}
