<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('facebook_id', 60)->nullable();
			$table->string('google_id', 60)->nullable();
			$table->integer('point_balance')->unsigned()->default(0);
			$table->float('geo_lat');
			$table->float('geo_lang');
			$table->string('phone_number', 120);
			$table->string('address_current', 512);
			$table->string('province_current', 512);
			$table->string('city_current', 512);
			$table->string('address_official', 120);
			$table->string('province_official', 512);
			$table->string('city_official', 512);
			$table->integer('review_count')->unsigned()->default(0);
			$table->decimal('rating',2,1)->unsigned()->default(0.0);
			$table->decimal('score')->unsigned();
			$table->integer('reputation')->unsigned()->default(0);

			$table->softDeletes();
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}