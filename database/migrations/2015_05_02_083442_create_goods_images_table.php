<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goods_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('goods_id')->unsigned();
			$table->string('image_url',256);
			$table->string('short_description',256);

			$table->foreign('goods_id')->references('id')->on('goods')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goods_images');
	}

}
