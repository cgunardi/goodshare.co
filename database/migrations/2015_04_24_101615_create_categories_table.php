<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parent_category_id')->unsigned()->nullable();
			$table->string('name', 128);
			$table->string('icon', 128)->nullable();
			$table->string('image_url', 128)->nullable();
			$table->boolean('is_hot')->default(false);
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('parent_category_id')->references('id')->on('categories')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
