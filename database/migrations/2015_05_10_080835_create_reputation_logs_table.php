<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReputationLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reputation_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('transaction_id')->unsigned()->nullable();
			$table->integer('related_user_id')->unsigned()->nullable();
			$table->integer('reputation');
			$table->char('type',1);

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('restrict');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
			$table->foreign('related_user_id')->references('id')->on('users')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reputation_logs');
	}

}
