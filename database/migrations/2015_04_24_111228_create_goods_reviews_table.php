<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goods_reviews', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('goods_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->tinyInteger('rating');
			$table->text('text');

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('goods_id')->references('id')->on('goods')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goods_reviews');
	}

}
