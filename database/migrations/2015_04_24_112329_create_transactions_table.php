<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('trans_number')->unsigned();
			$table->integer('goods_id')->unsigned();
			$table->integer('borrower_id')->unsigned();
			$table->date('date_start');
			$table->date('date_finish');
			$table->text('message');
			$table->char('state',1);
			$table->boolean('borrower_reviewed');
			$table->boolean('owner_reviewed');

			$table->softDeletes();
			$table->timestamps();

			$table->foreign('goods_id')->references('id')->on('goods')->onDelete('restrict');
			$table->foreign('borrower_id')->references('id')->on('users')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}

