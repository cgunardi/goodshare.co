<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goods', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('city_id')->unsigned()->nullable();
			$table->string('title', 256);
			$table->text('description');
			$table->integer('point');
			$table->float('geo_lat');
			$table->float('geo_lang');
			$table->integer('review_count')->insigned();
			$table->decimal('rating',2,1)->insigned();
			$table->integer('view_count')->unsigned()->default(0);
			$table->integer('transaction_count')->unsigned()->default(0);

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
			$table->foreign('city_id')->references('id')->on('cities')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goods');
	}

}
