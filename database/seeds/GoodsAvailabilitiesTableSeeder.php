<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Goods;
use App\GoodsAvailability;

class GoodsAvailabilitiesTableSeeder extends Seeder {
	
	public function run()
	{
		Model::unguard();

		$all_goods = Goods::all();

		$goods_review = 0;

		$state = 0;

		for ($i=0; $i < count($all_goods); $i++) { 
			$dt = Carbon::now();
			$goods = $all_goods[$i];

			for ($j=0; $j < 60; $j++) { 
				$dt->subDay();
				$state_change = rand(0,5)==0;
				if ($state_change) {
					$state++;
					if ($state>=3) $state = 0;
				};
				if ($state!=1) {
					GoodsAvailability::create([
						"date" => $dt->toDateString(),
						"goods_id" => $goods->id,
						"available" => ($state==0)
					]);
				};
			};
		};
	}

}
