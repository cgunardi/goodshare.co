<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		DB::table('user_reviews')->truncate();
		DB::table('point_logs')->truncate();
		DB::table('transactions')->truncate();
		DB::table('goods_reviews')->truncate();
		DB::table('goods_availabilities')->truncate();
		DB::table('goods')->truncate();
		DB::table('categories')->truncate();
		DB::table('password_resets')->truncate();
		DB::table('users')->truncate();

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		$this->call('UsersTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('GoodsTableSeeder');
		$this->call('GoodsAvailabilitiesTableSeeder');
		$this->call('GoodsReviewsTableSeeder');
		$this->call('TransactionsTableSeeder');
		$this->call('UserReviewsTableSeeder');
	}

}
