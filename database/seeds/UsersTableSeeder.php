<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\City;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		Model::unguard();

		$provinces = City::getProvinces();
		$cities = City::get();

		for ($i=0; $i < 10; $i++) { 

			$prov_current = $provinces[rand(0,count($provinces)-1)];
			$city_current = $cities[$prov_current][rand(0,count($cities[$prov_current])-1)];

			$prov_official = $provinces[rand(0,count($provinces)-1)];
			$city_official = $cities[$prov_official][rand(0,count($cities[$prov_official])-1)];

			User::create([
				'name' => "User ".$i,
				'email' => 'user'.$i.'@gmail.com',
				'password' => Hash::make('user'.$i),
				'geo_lat' => 0.0,
				'geo_lang' => 0.0,
				'phone_number' => "+628783222277".$i,
				'address_current' => "Jl. ".$city_current." no.".rand(0,200),
				'province_current' => $prov_current,
				'city_current' => $city_current,
				'address_official' => "Jl. ".$city_official." no.".rand(0,200),
				'province_official'=> $prov_official,
				'city_official' => $city_official,
				"point_balance" => 100000
			]);
		};
	}
}
