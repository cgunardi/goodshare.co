<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Goods;
use App\User;
use App\GoodsReview;

class GoodsReviewsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$all_goods = Goods::all();

		$all_user = User::all();

		$goods_review = 0;

		$user_id = 0;

		for ($i=0; $i < count($all_goods); $i++) { 
			$goods = $all_goods[$i];

			$review_count = rand(3,10);

			for ($j=0; $j < $review_count; $j++) { 
				GoodsReview::create([
					'goods_id' => $goods->id,
					'user_id' => $all_user[$user_id]->id,
					'rating' => rand(1,5),
					'text' => "Review-".$goods_review." of Goods-".$goods->id
				]);
				$goods_review++;
				$user_id++;
				if ($user_id>=count($all_user)) {
					$user_id = 0;
				};
			};
		};
	}

}
