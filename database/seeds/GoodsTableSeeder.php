<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Goods;
use App\User;

class GoodsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$cats = Category::all();

		$all_user = User::all();

		$goods_idx = 0;

		$user_id = count($all_user)-1;

		for ($i=0; $i < count($cats); $i++) { 
			$cat = $cats[$i];

			$item_count = rand(3,10);

			for ($j=0; $j < $item_count; $j++) { 
				Goods::create([
					'category_id' => $cat->id,
					'user_id' => $all_user[$user_id]->id,
					'title' => 'Good-'.$goods_idx,
					'description' => 'A good of Category-'.$cat->id,
					'point' => rand(5,50),
					'geo_lat' => (rand()-0.5)*100,
					'geo_lang' => (rand()-0.5)*100
				]);
				$goods_idx++;
				$user_id--;
				if ($user_id<0) {
					$user_id = count($all_user)-1;
				};
			};
		};
	}

}
