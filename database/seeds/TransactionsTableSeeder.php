<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Goods;
use App\Transaction;
use Carbon\Carbon;	

class TransactionsTableSeeder extends Seeder {

	public function run()
	{
		Model::unguard();

		$all_user = User::all();

		$all_goods = Goods::all();

		$user_idx = 0;

		for ($i=0; $i < count($all_goods); $i++) { 
			$goods = $all_goods[$i];

			$transactions = rand(5,15);

			for ($j=0; $j < $transactions; $j++) { 
				$user = $all_user[$user_idx];

				if ($user->id == $goods->user_id) {
					$user_idx++;
					if ($user_idx>=count($all_user)) {
						$user_idx = 0;
					};
				};

				$do = rand(0,5)>0;

				if ($do) {

					$trans = Transaction::create([
						'goods_id' => $goods->id,
						'borrower_id' => $user->id,
						'date_start' => Carbon::now()->subDay(rand(2,5))->toDateString(),
						'date_finish' => Carbon::now()->toDateString(),
						'message' => 'Transaction of Goods-'.$goods->id
					]);

					$approve = rand(0,5)>0;

					if ($approve) {
						$trans->transactionApprove();
						$trans->transactionAccept();
						$trans->transactionReturn();
					}
					else {
						$trans->transactionReject();
					}

					$user_idx++;
					if ($user_idx>=count($all_user)) {
						$user_idx = 0;
					};
					
				}
			};
		};
	}
}
