<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoriesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		for ($i=0; $i < 5; $i++) { 
			$cat = Category::create([
				'name' => "Category".$i
			]);
			$subcat = rand(0,3);
			for ($j=0; $j < $subcat; $j++) { 
				Category::create([
					'name' => "Category-".$i.'.'.$j,
					'parent_category_id' => $cat->id
				]);
			};
		};
	}

}
