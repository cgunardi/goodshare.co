<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Goods;

class GoodsImagesTableSeeder extends Seeder {
	
	public function run()
	{
		Model::unguard();

		$all_goods = Goods::all();

		$state = 0;

		for ($i=0; $i < count($all_goods); $i++) { 
			
			$goods = $all_goods[$i];
			$image_count = rand(2,5);

			for ($j=0; $j < $image_count; $j++) {
				GoodsImage::create([
					'goods_id' => $goods->id,
					'image_url' => Uuid::generate().".jpg",
					'short_description' => ""
				]);
			};
		};
	}

}
