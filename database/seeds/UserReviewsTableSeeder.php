<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\UserReview;

class UserReviewsTableSeeder extends Seeder {

	public function run()
	{
		Model::unguard();

		$user_all = User::all();

		for ($i=0; $i < count($user_all); $i++) { 

			for ($j=0; $j < count($user_all); $j++) { 

				$do = rand(0,3)>0;

				if (($do)&&($i!=$j)) {
					UserReview::create([
						'reviewer_id' => $user_all[$i]->id,
						'user_id' => $user_all[$j]->id,
						'rating' => rand(1,5),
						'text' => "Review to User-".$user_all[$j]->id." from ".$user_all[$i]->id
					]);
				}
				
			};
		};
	}
}
