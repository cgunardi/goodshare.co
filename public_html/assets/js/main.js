"use strict"; // Start of use strict
$(function () {
	$('.popover').popover();

	$('#ex1').slider({
		formatter: function(value) {
			return 'Current value: ' + value;
		}
	});
	$("#save-edit").click(function() {
		var gnames = $("#input-name").val();
		var points = $("#input-points").val();
		var description = $("#input-description").val();
		var availability = $("#input-availability").val();
		var meetingpoint = $("#input-meetingpoint").val();
		if((points!="")&&(gnames!="")&&(description!="")&&(availability!="")&&(meetingpoint!="")){
			$("#edit-name").text(gnames);
			$("#edit-points").text(points);
			$("#edit-description").text(description);
			$("#edit-availability").text(availability);
			$("#edit-meetingpoint").text(meetingpoint+" ("+lat+","+lng+")");
			$( "#alert-field" ).hide();
			$('#saves').modal('show');
		}else{
			$( "#alert-field" ).show();
		}
	});

	$('#rating-input').rating({
              min: 0,
              max: 5,
              step: 1,
              size: 'lg',
              showClear: false
       });
	$('#rating-input').on('rating.change', function() {
            console.log($('#rating-input').val());
        });
	$('.slider').slider();

    /* ION SLIDER */
    $("#range_1").ionRangeSlider({
        min: 0,
        max: 100,
        type: 'single',
        step: 1,
        postfix: " KM",
        prettify: false,
        hasGrid: true
    });
    $("#range_2").ionRangeSlider({
        min: 10,
        max: 1000,
        type: 'double',
        step: 10,
        postfix: " Points",
        prettify: false,
        hasGrid: true
    });
});